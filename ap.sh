#!/bin/bash
# as per https://ubuntuforums.org/showthread.php?t=1488953
# forum user: purelinuxuser
##TODO: use udhcpd
##TODO: use getops for God's sake!
##TODO: more variables (IP address and iptables shit)

# global is good enough:
BROADCAST="wlan0" # broadcasting interface
RECEIVE="wlan1" # receiving interface broadcast is connected to

if [[ $1 == "-0" || $1 == "--start" ]]
 then
 ## start hostapd
 echo "Starting hostapd"
 echo "You can view the log at /var/log/hostapd.log"

 # launch hostapd daemon
 hostapd -d /etc/hostapd/hostapd.conf > /var/log/hostapd.log &

 ## start dhcp server
 echo "Starting dhcpd3"

 # set IP address
 ifconfig $BROADCAST 192.168.0.2
 sleep 2

 # launch dhcpd3 daemon
## will have to adapt to use udhcpd and configure /etc/udhcpd.conf accordingly!
 echo "INTERFACES=$BROADCAST" > /etc/default/dhcp
 dhcpd3 $BROADCAST &

elif [[ $1 == "-1" || $1 == "--stop" ]]
 then
 # send signal 2 to hostapd and dhcpd3
 killall -2 hostapd dhcpd3

elif [[ $1 == "-2" || $1 == "--ics" ]]
 then
 # create iptables rules
 iptables -A FORWARD -i $RECEIVE -o $BROADCAST -s 192.168.0.2/24 -m conntrack --ctstate NEW -j ACCEPT
 iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
 iptables -A POSTROUTING -t nat -j MASQUERADE

 # set kernel variable(s)
 echo 1 > /proc/sys/net/ipv4/conf/all/forwarding

 # edit kernel configuration
 cp /etc/sysctl.conf /etc/sysctl.conf.ap_ctl
 echo "net.ipv4.conf.default.forwarding=1" >> /etc/sysctl.conf
 echo "net.ipv4.conf.all.forwarding=1" >> /etc/sysctl.conf

 # restart networking
 /etc/init.d/networking restart

elif [[ $1 == "-3" || $1 == "--noics" ]]
 then
 # remove iptables rules
 iptables -D FORWARD 1
 iptables -D FORWARD 1

 # set kernel variable(s)
 echo 0 > /proc/sys/net/ipv4/conf/all/forwarding

 # revert kernel configuration
 mv -i /etc/sysctl.conf.ap_ctl /etc/sysctl.conf

 # restart networking
 /etc/init.d/networking restart

else
 echo $0
 echo "A tool to manage hostapd and dhcpd3"
 echo "Usage:"
 echo "-0 --startStart hostapd and dhcpd3"
 echo "-1 --stopStop hostapd and dhcpd3 with signal 2"
 echo "-2 --icsActivate internet connection sharing"
 echo "between specified interfaces"
 echo "-3 --noicsUndo internet connection sharing settings"
fi

exit 0